using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    [SerializeField] private float _jumpForce;
    [SerializeField] private float _movementSpeed;
    [SerializeField] private Rigidbody2D _rigidbody2D;
    [SerializeField] private BoxCollider2D _boxCollider2D;
    [SerializeField] private AudioSource _jumpSfx;
    private float _levelWidth = 2.7f;
    private float _movement;
    private bool _isDead = false;

    public Action PlayerDied;

    public void Jump()
    {
        if (_isDead)
        {
            return;
        }
        _jumpSfx.Play();
        Vector2 vel = _rigidbody2D.velocity;
        vel.y = _jumpForce;
        _rigidbody2D.velocity = vel;
    }

    void Update()
    {
        if (_isDead)
        {
            return;
        }
        CheckBallPosition();
        if (Input.touchCount > 0)
        {

            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Moved)
            {
                _movement = touch.deltaPosition.x * _movementSpeed;
            }
        }
        else
        {
            _movement = 0;
        }
    }

    private void FixedUpdate()
    {
        Vector2 vel = _rigidbody2D.velocity;
        vel.x = _movement;
        _rigidbody2D.velocity = vel;
    }

    public void CheckBallPosition()
    {
        if(transform.position.x > _levelWidth)
        {
            transform.position -= new Vector3(2 * _levelWidth, 0, 0);
        }
        else if (transform.position.x < -_levelWidth)
        {
            transform.position += new Vector3(2 * _levelWidth, 0, 0);
        }
    }

    public void Dead()
    {
        _isDead = true;
        _boxCollider2D.enabled = false;
        PlayerDied?.Invoke();
    }
}
