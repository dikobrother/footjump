using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : MonoBehaviour
{
    [SerializeField] private GameObject _platform;
    [SerializeField] private GameObject _killPlatform;
    [SerializeField] private GameObject _startPlatform;
    [SerializeField] private float _killRate;
    [SerializeField] private int _startPlatformsCount;
    [SerializeField] private float _minYoffset;
    [SerializeField] private float _maxYoffset;
    [SerializeField] private float _minXoffset;
    [SerializeField] private float _maxXoffset;
    private List<GameObject> _platforms = new List<GameObject>();
    private float _lastSpawnY;

    private void Awake()
    {
        _lastSpawnY = _startPlatform.transform.position.y;
        _platforms.Add(_startPlatform);
        GenetateStartPlatforms();
    }
    
    private void GenetateStartPlatforms()
    {
        for (int i = 0; i < _startPlatformsCount; i++)
        {
            _lastSpawnY += Random.Range(_minYoffset, _maxYoffset);
            GameObject platform = Instantiate(_platform, new Vector3(Random.Range(_minXoffset, _maxXoffset), _lastSpawnY, 0), Quaternion.identity);
            _platforms.Add(platform);
        }
    }

    public void CreatePlatform()
    {
        float random = Random.Range(0f, 1f);
        if(random < _killRate && !_platforms[_platforms.Count - 1].TryGetComponent(out KillPlatform killPlatform) && !_platforms[_platforms.Count - 2].TryGetComponent(out KillPlatform killPlatform2) && !_platforms[_platforms.Count - 3].TryGetComponent(out KillPlatform killPlatform3))
        {
            _lastSpawnY += Random.Range(_minYoffset, _maxYoffset);
            GameObject platform = Instantiate(_killPlatform, new Vector3(Random.Range(_minXoffset, _maxXoffset), _lastSpawnY, 0), Quaternion.identity);
            _platforms.Add(platform);
        }
        else
        {
            _lastSpawnY += Random.Range(_minYoffset, _maxYoffset);
            GameObject platform = Instantiate(_platform, new Vector3(Random.Range(_minXoffset, _maxXoffset), _lastSpawnY, 0), Quaternion.identity);
            _platforms.Add(platform);
        }
    }

    public void DestroyFirstPlatform()
    {
        GameObject platform = _platforms[0];
        _platforms.Remove(platform);
        Destroy(platform);
        CreatePlatform();
    }
}
