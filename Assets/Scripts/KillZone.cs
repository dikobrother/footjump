using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillZone : MonoBehaviour
{
    [SerializeField] private LevelGenerator _levelGenerator;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.TryGetComponent(out Ball ball))
        {
            ball.Dead();
        }
        else
        {
            _levelGenerator.DestroyFirstPlatform();
        }
    }
}
