using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioController : MonoBehaviour
{
    [SerializeField] private AudioMixer _mixer;

    private void Awake()
    {
        UpdateAudio();
    }

    public void UpdateAudio()
    {
        var game = SaveSystem.LoadData<GamesSaveData>();
        UpdateMusic(game.IsMusic);
        UpdateSound(game.IsSound);
    }

    private void UpdateMusic(bool active)
    {
        if (active)
        {
            _mixer.SetFloat("MusicVolume", 0);
        }
        else
        {
            _mixer.SetFloat("MusicVolume", -80);
        }
    }

    private void UpdateSound(bool active)
    {
        if (active)
        {
            _mixer.SetFloat("SoundVolume", 0);
        }
        else
        {
            _mixer.SetFloat("SoundVolume", -80);
        }
    }
}
