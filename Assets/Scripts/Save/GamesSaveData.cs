using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class GamesSaveData : SaveData
{
    public int Score;
    public bool IsMusic;
    public bool IsSound;

    public GamesSaveData(int score, bool isMusic, bool isSound)
    {
        Score = score;
        IsMusic = isMusic;
        IsSound = isSound;
    }
}
