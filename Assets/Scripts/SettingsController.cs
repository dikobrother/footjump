using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsController : MonoBehaviour
{
    [SerializeField] private AudioController _audioController;
    [SerializeField] private List<Sprite> _sprites;
    [SerializeField] private List<Image> _images;

    private void OnEnable()
    {
        UpdateSettings();
    }

    private void UpdateSettings()
    {
        var game = SaveSystem.LoadData<GamesSaveData>();
        UpdateMusic(game.IsMusic);
        UpdateSound(game.IsSound);
        _audioController.UpdateAudio();
    }

    public void UpdateMusic(bool active)
    {
        if (active)
        {
            _images[0].sprite = _sprites[0];
        }
        else
        {
            _images[0].sprite = _sprites[1];
        }
    }

    public void UpdateSound(bool active)
    {
        if (active)
        {
            _images[1].sprite = _sprites[0];
        }
        else
        {
            _images[1].sprite = _sprites[1];
        }
    }

    public void OnMusicButtonClicked()
    {
        var game = SaveSystem.LoadData<GamesSaveData>();
        game.IsMusic = !game.IsMusic;
        SaveSystem.SaveData(game);
        UpdateSettings();
    }

    public void OnSoundButtonClicked()
    {
        var game = SaveSystem.LoadData<GamesSaveData>();
        game.IsSound = !game.IsSound;
        SaveSystem.SaveData(game);
        UpdateSettings();
    }
}
