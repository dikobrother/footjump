using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private GameObject _mainCanvas;
    [SerializeField] private GameObject _settingsCanvas;
    [SerializeField] private TMP_Text _scoreText;

    private void Awake()
    {
        var game = SaveSystem.LoadData<GamesSaveData>();
        _scoreText.text = game.Score.ToString();
    }

    public void StartGame()
    {
        SceneManager.LoadScene("GameScene");
    }

    public void OpenSettings()
    {
        _mainCanvas.SetActive(false);
        _settingsCanvas.SetActive(true);
    }

    public void CloseSettings()
    {
        _mainCanvas.SetActive(true);
        _settingsCanvas.SetActive(false);
    }
}
