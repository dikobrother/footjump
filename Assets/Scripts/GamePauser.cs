using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GamePauser : MonoBehaviour
{
    [SerializeField] private GameObject _pauseCanvas;
    [SerializeField] private GameObject _settingsCanvas;

    public void OnPauseButtonClick()
    {
        Time.timeScale = 0;
        _pauseCanvas.SetActive(true);
    }

    public void OnUnpauseButtonClick()
    {
        Time.timeScale = 1;
        _pauseCanvas.SetActive(false);
    }

    public void OnExitButtonClick()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("MainMenu");
    }

    public void OnSettingsButtonClick()
    {
        _pauseCanvas.SetActive(false);
        _settingsCanvas.SetActive(true);
    }

    public void OnCloseSettingsButtonClick()
    {
        _pauseCanvas.SetActive(true);
        _settingsCanvas.SetActive(false);
    }
}
