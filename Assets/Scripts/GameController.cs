using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine;

public class GameController : MonoBehaviour
{
    [SerializeField] private Ball _player;
    [SerializeField] private GameObject _gameOverCanvas;
    [SerializeField] private Transform _camera;
    [SerializeField] private TMP_Text _scoreText;
    [SerializeField] private TMP_Text _loseScoreText;
    [SerializeField] private TMP_Text _recordText;
    [SerializeField] private GameObject _gameCanvas;

    private void OnEnable()
    {
        _player.PlayerDied += OnPlayerDead;
    }

    private void OnDisable()
    {
        _player.PlayerDied -= OnPlayerDead;
    }

    private void Update()
    {
        _scoreText.text = ((int)(_camera.transform.position.y * 10)).ToString();
    }

    private void OnPlayerDead()
    {
        _gameOverCanvas.SetActive(true);
        _gameCanvas.SetActive(false);
        var game = SaveSystem.LoadData<GamesSaveData>();
        if(game.Score < (int)(_camera.transform.position.y * 10))
        {
            game.Score = (int)(_camera.transform.position.y * 10);
            SaveSystem.SaveData(game);
        }
        _loseScoreText.text = ((int)(_camera.transform.position.y * 10)).ToString();
        _recordText.text = SaveSystem.LoadData<GamesSaveData>().Score.ToString();
    }

    public void RestartGame()
    {
        SceneManager.LoadScene("GameScene");
    }

    public void ExitGame()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
